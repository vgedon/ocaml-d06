
module type FIXED = sig   (* Return struct signature *)
	type t
	val of_float  : float -> t
	val of_int    : int -> t
	val to_float  : t -> float
	val to_int    : t -> int
	val to_string : t -> string
	val zero : t
	val one  : t
	val succ : t -> t
	val pred : t -> t
	val min  : t -> t -> t
	val max  : t -> t -> t
	val gth  : t -> t -> bool
	val lth  : t -> t -> bool
	val gte  : t -> t -> bool
	val lte  : t -> t -> bool
	val eqp  : t -> t -> bool			(** physical equality   *)
	val eqs  : t -> t -> bool			(** structural equality *)
	val add  : t -> t -> t
	val sub  : t -> t -> t
	val mul  : t -> t -> t
	val div  : t -> t -> t
	val foreach : t -> t -> (t -> unit ) -> unit
end

module type FRACTIONNAL_BITS = sig val bits : int end


module MakeFractionnalBits =
	functor (Fractionnal_bits : FRACTIONNAL_BITS) ->
		struct
			let bits = Fractionnal_bits.bits
		end

module type MAKE = 
	functor (Fractionnal_bits : FRACTIONNAL_BITS) -> FIXED

module Make : MAKE =
functor (Fractionnal_bits : FRACTIONNAL_BITS) ->
	struct
		type t = int
		let of_int i 		= i lsl Fractionnal_bits.bits
		let to_int t 		= t lsr Fractionnal_bits.bits
		let zero 			= of_int 0
		let one  			= of_int 1
		let of_float f 		= truncate (ceil (f *. (2. ** float_of_int(Fractionnal_bits.bits))))
		let to_float t 		= (float_of_int t) /. (float_of_int one)
		let to_string t 	= string_of_float(to_float t)
		let succ t = t + 1
		let pred t = t - 1
		let min tl tr = if tl < tr then tl else tr
		let max tl tr = if tl > tr then tl else tr
		let gth tl tr = tl > tr
		let lth tl tr = tl < tr
		let gte tl tr = tl >= tr
		let lte tl tr = tl <= tr
		let eqp tl tr = tl = tr			(** physical equality   *)
		let eqs tl tr = tl == tr			(** structural equality *)
		let add tl tr = tl + tr
		let sub tl tr = tl - tr
		let mul tl tr = tl * tr
		let div tl tr = tl / tr
		let foreach s e f = 
		let rec loop s e f op = 
			match s with
				| s when s <> e -> f s ; loop (op s) e f op
				| _ -> f e
			in
			match s with
			| s when s >= e -> loop s e f pred
			| _ -> loop s e f succ
	end

module Fixed4 : FIXED = Make ( struct let bits = 4 end )
module Fixed8 : FIXED = Make ( struct let bits = 8 end )


let () =
	let x8 = Fixed8.of_float 21.10 in
	let y8 = Fixed8.of_float 21.32 in
	let r8 = Fixed8.add x8 y8 in
	let zero = Fixed4.zero in
	let one = Fixed4.one in
	let t1 = Fixed4.of_float 42.42 in
	let t2 = Fixed4.of_float 42. in
	let t3 = Fixed4.of_int 21 in
	print_endline (Fixed8.to_string r8);
	print_endline "foreach Fixed4.zero to Fixed4.one : ";
	Fixed4.foreach zero one (fun f -> print_endline (Fixed4.to_string f));
	print_endline "\nforeach Fixed4.one to Fixed4.zero : ";
	Fixed4.foreach one zero (fun f -> print_endline (Fixed4.to_string f));
	print_endline "\nforeach Fixed4.one to Fixed4.one : ";
	Fixed4.foreach one one (fun f -> print_endline (Fixed4.to_string f));
	print_string "\ntest Fixed4.eqp Fixed4.of_float 42.42 Fixed4.of_float 42.42 : ";
	print_string (string_of_bool(Fixed4.eqp t1 t1));
	print_string "\ntest Fixed4.eqp Fixed4.of_float 42. Fixed4.of_float 42.42 : ";
	print_string (string_of_bool(Fixed4.eqp t2 t1));
	print_string "\ntest Fixed4.eqs Fixed4.of_float 42. Fixed4.of_int 42 : ";
	print_string (string_of_bool(Fixed4.eqs t2 (Fixed4.of_int 42)));
	print_string "\ntest Fixed4.eqs Fixed4.of_float 42. Fixed4.of_float 42.42 : ";
	print_string (string_of_bool(Fixed4.eqs t2 t1));
	print_string "\ntest Fixed4.min Fixed4.of_int 21 Fixed4.of_float 42.42 : ";
	print_string (Fixed4.to_string(Fixed4.min t3 t1));
	print_string "\ntest Fixed4.max Fixed4.of_int 21 Fixed4.of_float 42.42 : ";
	print_string (Fixed4.to_string(Fixed4.max t3 t1));
	print_string "\ntest Fixed4.add Fixed4.of_int 21 Fixed4.of_float 21.42 : ";
	print_string (Fixed4.to_string(Fixed4.add t3 (Fixed4.of_float 21.42)));
	print_string "\ntest Fixed4.sub Fixed4.of_int -21 Fixed4.of_float -21.42 : ";
	print_string (Fixed4.to_string(Fixed4.sub (Fixed4.of_int (-21)) (Fixed4.of_float (-21.42))));
	print_string "\ntest Fixed4.mul Fixed4.of_float 21.42 Fixed4.of_float 21.42 : ";
	print_string (Fixed4.to_string(Fixed4.mul (Fixed4.of_float 21.42) (Fixed4.of_float 21.42)));
	print_string "\ntest Fixed4.div Fixed4.of_float 42.42 Fixed4.of_int 2 : ";
	print_endline (Fixed4.to_string(Fixed4.div t1 (Fixed4.of_int 2)));